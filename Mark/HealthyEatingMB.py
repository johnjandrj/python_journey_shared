# HealthyEatingMB.py

# Mark's version. 01/05/20
# Part 1, get the name of 5 items of fruit and veg.
# Part 2, rather than a numeric prompt use the words in the 'prompt' table when asking the user for each item. 
# Part 3, print out the myfav list as a sentence ie. item1, item2, item3, item 4 and item5
# Updated to do 7 a day.

# Words to us when prompting the user for input
prompt=['First','Second','Third','Fourth','Fifth','Sixth','Seventh']

# Recomended portions of fruit and veg abbrivated to 'rec'
# I have put the recomended quantity in a variable and it's value tracks the number of prompt words.
rec=len(prompt)

#List to hold user entry, abbrivated fruit and veg to 'fav'
myfav=[]

print('Please enter the',str(rec),'fruit and veg you have had tody;')
for item in prompt:
    print(item,'item:',end=" ")
    user_input=input()
    myfav.append(user_input)
    
print(myfav)
fav='Your fruit and veg today included '

len_myfav = len(myfav)
for item in range(0,len_myfav):
    if item < len_myfav -2:
        fav=fav + myfav[item] + ', '
    elif item < len_myfav - 1:
        fav=fav + myfav[item] + ' and ' 
    else:
        fav=fav + myfav[item] + '.'
        
print(fav)

