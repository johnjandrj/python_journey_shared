# Work out date from day of the year.
# File name DateFromDay2.py
#
# Mark 11/09/20 23:00  v0.1
#---------------------------------------------------------------------
#Peter's warm up problems. No.??
#
#Write a program which accepts user input of an integer between 1 and
#365 and returns the date of that day in 2021, where 1st January is
#day 1. 
#
#For example, the input 132 should result in the output
#
#Wednesday, 12th May
#---------------------------------------------------------------------

# For 2021 first day of the years is a Friday.

dicDays={ 1:'Friday',
          2:'Saturday',
          3:'Sunday',
          4:'Monday',
          5:'Tuesday',
          6:'Wednesday',
          0:'Thursday',
          }

# sequence to find month according to how many days of the year have passed.
# So 334 is the total number of days in the months Jan to Nov

listMonths=[(334,'Dec'),
            (304,'Nov'),
            (273,'Oct'),
            (243,'Sep'),
            (212,'Aug'),
            (181,'Jul'),
            (151,'Jun'),
            (120,'May'),
            ( 90,'Apr'),
            ( 59,'Mar'),
            ( 31,'Feb'),
            (  0,'Jan'),
            ]

dicSuffix={ 1:'st',
            2:'nd',
            3:'rd',
            4:'th',
            5:'th',
            6:'th',
            7:'th',
            8:'th',
            9:'th',
            0:'th',
           }


dayOfYear=int(input('Enter the day of the year: '))

#print(str(dayOfYear % 7))
dayOfWeek=dicDays[dayOfYear % 7]

for tday,month in listMonths:
    if dayOfYear > tday:
        day=dayOfYear - tday
        break

#print(str(day)[-1:])
#suffix=dicSuffix[str(day)[-1:]] # dic key as string
suffix=dicSuffix[day % 10]       # dic key as number

text=dayOfWeek+' '+str(day)+suffix+' '+month

print(text)


