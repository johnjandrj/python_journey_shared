#!/usr/bin/python3

# Script name: mug_factory.py

# Mark Bradley
# 10/06/20

# demo of a class object built to show the use of a commom tea mug
# kept as simple as possible to show the using a class to model a real world object.

class mug:
    ''' Class to describe the common Mug.
        Size is capacity in ml.
        Decoration,colour picture etc.
        State how many ml are in the mug at the moment.
        Clean, True if the mug has not been used
        Content, whats in the mug'''
    
    def __init__(self,size=350,decoration=None,state=0,clean=True):
        # Initalize an instance of mug
        # This builds the 'mug' using the default parameter unless new ones are supplied.
        self.size=size
        self.decoration=decoration
        self.state=state
        self.clean=clean
        self.content='nothing'      # Mugs have no contentat the start
        
    def fill(self,quantity,content='Tea'):
        # quantity is the amount to put in the mug. It cannot exceed the size!
        self.content=content
        
        if quantity > (self.size - self.state):
            print('Oh dear some of that over flowed!')
            self.state = self.size
        else:
            self.state = quantity
            
        self.clean = False
        
    def empty(self):
        # Pour away the contents - state=0
        self.state=0
        
    def sip(self,sip_size=30):
        # Take a sip of the drink, default is 30ml
        if sip_size > self.state:
            print('Oh dear all your '+self.content+' has gone!')
            self.state = 0
        else:
            self.state = self.state - sip_size
    
    def wash(self):
        self.empty()
        self.clean=True
        
    def whatsleft(self):
        # Print the current state of the mug
        print('The '+self.decoration+' mug has '+str(self.state)+' ml of '+self.content+' in it')
    
    
# Tried this first but it's not what I wanted.  
# mug1_class = type('Mark', (mug,), {})  # This creates a new class, not a class object

visitors=['john','rose','bill','dave']  # Visitors already present

# Supply of clean mugs available
clean_mugs=[
    (350,'poppy'),
    (300,'bird'),
    (400,'train'),
    (375,'flower'),
    (500,'horse'),
    (325,'boat'),
    ]

# Mugs allocated to visitors 
in_use_mugs=[]

# Dictionary linking visitors to mug instances
allocation={}

    # Allocation of mugs to visitors
def mug_allocator(name):
    next_mug=clean_mugs.pop()# allocates in rev order-put 0 in parm work forwards
    size,decor=next_mug
    allocation[name]=mug(size,decor)
    in_use_mugs.append(next_mug)
    

# Create a mug instance for each visitor and allocate a mug.
# Mugs may be referenced by name.

for each_visitor in visitors:
    mug_allocator(each_visitor)
    
print('Johns mug holds',allocation['john'].size)

# Work out how much tea is needed to fill the allocated mugs
tea_reqd=0
for each_mug in allocation:
    print(each_mug)
    tea_reqd+=allocation[each_mug].size
    
print(tea_reqd,'ml of tea needed')

# Fill the mugs
for each_visitor in allocation:
    allocation[each_visitor].fill(allocation[each_visitor].size)

# Display current state
for key, each_mug in allocation.items():
    print(key,end=' ')
    each_mug.whatsleft()

# Give a mug to a new arrival and fill it as required
new_arrival=input('Enter another visitors: ')
mug_allocator(new_arrival)
new_arr_drink=input('What would you like ? ')
allocation[new_arrival].fill(allocation[new_arrival].size,new_arr_drink)

# Display current state
for key, each_mug in allocation.items():
    print(key,end=' ')
    each_mug.whatsleft()


