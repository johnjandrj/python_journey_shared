#!/usr/bin/python3

import sys

rng = range(4,8)
numberlst = [4,5,6,7]
stringlst = ["four","five","six","seven"]
numberdict = {
    1:4,
    2:5,
    3:6,
    4:7,
}
numbertuple = (4,5,6,7)

print(sys.getsizeof(rng))
print(sys.getsizeof(numberlst))
print(sys.getsizeof(stringlst))
print(sys.getsizeof(numberdict))
print(sys.getsizeof(numbertuple))

print("-"*30)

hugerng = range(0,10000)
hugelist = list(hugerng)

print(sys.getsizeof(hugerng))
print(sys.getsizeof(hugelist))