#!/usr/bin/python3

# Limit the size of the grid so that it has a chance of fitting on the screen

def build_with_tail(stringthing, n, finishing_touch):
    return stringthing * n + finishing_touch

limit = 78

reasonable_values = False

while not reasonable_values:
    brick = input("Brick (character or short character string)?: ")
    cell_size = int(input("Cell Size (small positive integer)?:  "))
    grid_size = int(input("Grid Size (small positive integer)?: "))

    brick_size = len(brick)

    if grid_size * (cell_size + brick_size) < limit:
        reasonable_values = True
        print()
    else:
        print("These values are too big - please try again")

anchor = brick
beam = build_with_tail(brick, cell_size, brick)
# print("beam:\n"+beam)
structural_row =  anchor + build_with_tail(beam, grid_size, "\n")
# print("structural_row:\n"+structural_row)
cell_beam = build_with_tail(" " * brick_size, cell_size, brick)
# print("cell_beam:\n"+cell_beam)
cell_row =  anchor + build_with_tail(cell_beam, grid_size, "\n")
# print("cell_row:\n"+cell_row)

grid_row = structural_row * brick_size + build_with_tail(cell_row, cell_size, "")
# print("grid_row:\n"+grid_row)
grid = build_with_tail(grid_row, grid_size, structural_row * brick_size)
# print("grid:\n"+grid)
print(grid)
