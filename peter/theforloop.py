#!/usr/bin/python3

rng = range(4,8)
numberlst = [4,5,6,7]
stringlst = ["four","five","six","seven"]
numberdict = {
    1:4,
    2:5,
    3:6,
    4:7,
}
numbertuple = (4,5,6,7)
charstring = "4567"

print("range")
for item in rng:
    print(item)

print("list of numbers")
for item in numberlst:
    print(item)

print("tuple of numbers")
for item in numbertuple:
    print(item)

print("list of strings")
for item in stringlst:
    print(item)

print("dictionary of numbers with numeric keys.   Here are the keys:")
for item in numberdict:
    print(item)

print("dictionary of numbers with numeric keys.   Here are the values:")
for item in numberdict:
    print (numberdict[item])

print("dictionary of numbers with numeric keys.   Here are both the keys and the values:")
for key, value in numberdict.items():
  print(key, value) 

print("string")
for item in charstring:
    print (item)
