#!/usr/bin/python3

import sys
import math

a = 3

b = 3

print(a == b)
print(a is b)

print(sys.getsizeof(3))
print(sys.getsizeof(a))
print(sys.getsizeof(b))

a = 4
print(a == b)
print(a is b)

print(sys.getsizeof(3))
print(sys.getsizeof(a))
print(sys.getsizeof(b))

c = 12312312479783425349872345972983742387682340988082349768689126340928340982349612340982341892736410234091283407129347918237491283479712638461283746023480912834982734597984725892347598772345927349577349587234957293487532495793847577345
print(c)
print(sys.getsizeof(c))

astr = "Skiddaw"
print(sys.getsizeof("Skiddaw"))
print(sys.getsizeof(astr))

bstr = "The fourth highest mountain in England"
print(sys.getsizeof(bstr))

print(astr[3])
print(bstr[3])

cstr = "A long string\nwhich includes\na number of newlines\t\tand two tabs"
print(sys.getsizeof(cstr))
print("-----------------")
print(cstr[13])
print("-----------------")

position = int(input('Enter character position: '))
print("Character at position", position, "is <", cstr[position],">")

for position in range(0,len(cstr)):
    print("Character at position", position, "is <", cstr[position],">")

print('\t')



