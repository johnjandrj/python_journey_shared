#!usr/bin/python 3
#HangmanJJ game v computer generated word uses hangman_words.txt as word source.
#John Johnson with "getletter" from PN
#200606

from random import random

new_word_list=[]
count=int()
totlet=[]
string_hang_word=""
letter=""
string_ans_word=""
leng=int()
ans="y"

def getword(guess_word,secret_word):                    # gets a random word from text file,                                        #initialises guess word  
    num=int(random()*leng) 
    secret_word=new_word_list[num]         
    lenw=len(secret_word)
    guess_word=lenw*"-"
    print(secret_word)
    print(guess_word)                                   #Comment back in/out for testing
    return guess_word, secret_word


def getletter():
    msg=" " 
    while len(msg)>0:
        if len(msg)!= 1: print(msg)
        new_letter=input("Enter a letter  ")
        new_letter=new_letter.upper()
        if len(new_letter)!=1:msg="single letter only"
        elif totlet.count(new_letter)>0: msg="Letter already used"            
        elif not 64<ord(new_letter)<91: msg='Please enter a letter between "A" and "Z"'
        else: msg=""
    return new_letter

def checkandupdate(build, bad_guesses):  
    build=""    
    lengw=len(string_hang_word)    
    for ind in range(lengw):      
        if string_hang_word[ind]!= letter:              #loop through and compare           
            build = build+string_ans_word[ind]
        else:  
            build= build + letter
    if string_ans_word==build:                          #condition for wrong guess
        bad_guesses+=1    
    return build, bad_guesses

def stringtotlet(letters_used):                         #converts 'list' of letters used so far to 
    letters_used=""                                     #'string for presentation    
    lengw=len(totlet)
    for x in range(lengw):
        letters_used=letters_used+(totlet[x])+" "
    return letters_used

#---------------------------------------------------------------------------------------------------

filename = 'hangman_words.txt'
with open(filename,'r') as reader:
    whole_text = reader.read()
    whole_text = whole_text.upper()
    word_list = whole_text.split()        
for word in word_list:            
    new_word_list.append(word)
leng=len(new_word_list)       
while ans == "y":
    string_tuple=getword(string_ans_word, string_hang_word)
    string_ans_word=string_tuple[0]
    string_hang_word=string_tuple[1]                         #get a hidden word and initialise answer word
    while string_hang_word!=string_ans_word and count<10:    #criteria for finishing          )
        letter=getletter()                                   #fetch another letter
        string_tuple=checkandupdate(string_ans_word, count)
        string_ans_word=string_tuple[0]
        count=string_tuple[1]                                #test new letter v hidden, update answer word   
        totlet.append(letter)                                #update and sort letters already used 
        totlet.sort()
        string_totlet=stringtotlet(totlet)                   #convert 'list to string' for presentation
        print(string_ans_word, "\t\t\t\t", string_totlet,"\t\t\t\t\t\t",count, "bad guesses\n")#update status on screen        
    if count <10:                                            # messages to screen on completion or failed
        print("YOU WIN!! The word is indeed", string_ans_word)
    elif count>=10:
        print("You lose, the word is ",string_hang_word,"\n")
        print("You took",count,"bad guesses to complete the puzzle.\n")  
    print("Thank-you for playing\n")
    count=0
    totlet=[]
    ans = input("To play again Enter 'y'" )
    print()







