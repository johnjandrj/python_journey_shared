# hangman.py
# Team version.
# 4/6/2020
# Edit by Mark 7/6/20
# Added graphic scaffold and check on repeat letter entry.
# While loop to allow play again.
# Hangman game.

# Import modules
import random

#--Text elements to draw scafold --
s1='--------'
s2=' |\n'
s3=' -----\n'
s4=' |   |\n'
s5=' |   O\n'
s6=' |  /|\n'
s6a=' |  /|\\\n'
s7=' |   |\n'
s8=' |  / \n'
s8a=' |  / \\\n'
#--List holding all the stages in the scaffold build--
# Note they are in reverse order to match count down of guesses
scaffold=[
    (s3 + s4 + s5 + s6a + s7+ s8a + s2 * 2 + s1),
    (s3 + s4 + s5 + s6a + s7+ s8 + s2 * 2 + s1),
    (s3 + s4 + s5 + s6a + s7 + s2 * 3 + s1),
    (s3 + s4 + s5 + s6a + s2 * 4 + s1),
    (s3 + s4 + s5 + s6 + s2 * 4 + s1),
    (s3 + s4 + s5 + s2 * 5 + s1),
    (s3 + s4 + s2 * 6 + s1),
    (s3 + s2 * 7 + s1),
    (s2 * 7 + s1),
    (s1),
    '',
    ]

def get_word_list(filename):
    #Function to load words from file
    
    with open(filename,'r') as reader:
        whole_text = reader.read()
        
    return whole_text.split()
    
def get_word(words):
    #Select a secret word from the list
    index  = random.randint(0, len(words)-1)
    return words[index]

def user_entry():
    # Get a single character from the user. Convert it to lower case
    flag_letterentry=True
    while flag_letterentry:
        char=input('Enter a letter:')
        char=char.lower()
        if len(char)!=1:
            print('Please enter just 1 letter')
        elif 96 < ord(char) < 123:  # Check character between 'a' and 'z'
            flag_letterentry=False  # Entry valid so set flag to exit loop
        else:
            print('Please enter a letter between "a" and "z"')
    return char
            

#------------------------- Game Play -------------------------------

words_list=get_word_list('nouns.txt') # Load the words from file, only reqd once.
playagain_flag=True
    
while playagain_flag:
    # Game variables
    wrong_guesses=10
    correct_guesses=''    # Letters that the user has entered that are in the secret word
    incorrect_guesses=[]  # Letters that the user has entered that are not in the secret word

    
    secret_word=get_word(words_list)
    print(secret_word)
    prompt='_ ' * len(secret_word)
    print(prompt)
    word_complete_flag=False
    
    while wrong_guesses>0 and not word_complete_flag:
        
        used_already_flag=True
        while used_already_flag:
            guess=user_entry()
            if guess in incorrect_guesses or guess in correct_guesses:
                print('You have tried that letter already')
            else:
                used_already_flag=False
            
        if guess in secret_word:
            correct_guesses=correct_guesses + guess
            word_complete_flag=True
            
            prompt=''
            for letter in secret_word:
                if letter in correct_guesses:
                    prompt=prompt+letter+' '
                    
                else:
                    prompt=prompt+'_ '
                    word_complete_flag=False
                                
        else:
            incorrect_guesses.append(guess)
            wrong_guesses-=1
            print(scaffold[wrong_guesses])
            
                    
        if len(incorrect_guesses)>0:            
            print('Incorrect letters tried so far:',end='')
            for letter in incorrect_guesses:
                print(letter,end=' ')
            print(' Guesses left:',wrong_guesses)
            
        print(prompt,'\n')
            

    if word_complete_flag:
        print('Well done you won! The word is',secret_word)
    else:
        print("Oh dear you've had it!!!! The word was",secret_word)
        
    again=input('Do you want to play again ? Y/N ')
    if again.upper() !='Y':
        playagain_flag=False
        
print('All Done!')
