# The game of Hangman

1. I provide a word.
2. I tell the player how many letters in the word by drawing the "grid" - the correct number of underscore characters.
3. The player guesses a letter.
4. I tell them whether they've already used it (!).
5. I check to see if it's in the word, and which character position(s).
6. If it is in, I fill in the grid with that letter in the correct position(s); if it's not in, I add an element to the hanged person drawing.
7. If the word has now been guessed completely, the player has won; if the hanged person drawing is now complete, I've won; if neither of the above, we go back to step 3.

So the **data objects** I need to keep track of what's going on are:

* The word;
* The "grid", which will stay the same size but whose content will change over time as letters are added in the right places;
* The letter the player guesses;
* A "list" of letters guessed so far;
* A counter of how many failed attempts (ie how many elements on the hanged person drawing)

I'll also need to know how many elements make up the complete hanged person, since this is a condition I'll be testing for in step 7.









