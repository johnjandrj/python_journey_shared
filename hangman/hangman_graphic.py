# hangman_graphic.py
# Mark 5/6/20
# Uses text characters to represent hanged man for the hangman game.
# Strings 6,6a and 8,8a allow variations depending on if 8 or 10 wrong guesses are allowed.

s1='--------'
s2=' |\n'
s3=' -----\n'
s4=' |   |\n'
s5=' |   O\n'
s6=' |  /|\n'
s6a=' |  /|\\\n'
s7=' |   |\n'
s8=' |  / \n'
s8a=' |  / \\\n'
   
print(s1)
n=input('press a key')
print(s2 * 7 + s1)
n=input('press a key')
print(s3 + s2 * 7 + s1)
n=input('press a key')
print(s3 + s4 + s2 * 6 + s1)
n=input('press a key')
print(s3 + s4 + s5 + s2 * 5 + s1)
n=input('press a key')
print(s3 + s4 + s5 + s6 + s2 * 4 + s1)
n=input('press a key')
print(s3 + s4 + s5 + s6a + s2 * 4 + s1)
n=input('press a key')
print(s3 + s4 + s5 + s6a + s7 + s2 * 3 + s1)
n=input('press a key')
print(s3 + s4 + s5 + s6a + s7+ s8 + s2 * 2 + s1)
n=input('press a key')
print(s3 + s4 + s5 + s6a + s7+ s8a + s2 * 2 + s1)



