# # hyphenated_word.py
# Mark 29/5/20
# A selection of different methods for extracting the hyphenated words
# in the nouns.txt file

filename = 'nouns.txt'      #Windows
#filename = './nouns.txt'      #Linux

with open(filename,'r') as reader:
    whole_text = reader.read()
words_list = whole_text.split()
reader.close


# for loop
#------------
hy_words_f=[]
for word in words_list:
    if '-' in word:
        hy_words_f.append(word)
        
        print(word)
        
print('for:',len(hy_words_f))

# list comprehension
#-------------------
hy_words_lc=[word for word in words_list if '-' in word]

print('list comprehension:',len(hy_words_lc))

# list filter
#---------------

hy_words_filter=list(filter((lambda word: '-' in word),words_list))

print('list filter:',len(hy_words_filter))

# Regular expression
#-------------------
# re works on strings not lists so works through whole_text

import re
pattern=re.compile('\S*[-]\S*[-]\S*')  # Find double hyphenated words
re_list = re.findall(pattern, whole_text)
print('regular expression:',len(re_list))
print(re_list)

'''
re_list_iter=re.finditer(pattern, whole_text)
try:
    while True:
        print(next(re_list_iter))
            
except StopIteration:
    pass
'''

# Fuction attribute
# Creating an antribute for a function that then can be used like
# a static local variable with the function.

def attribute(x,y):
    
    attribute.count+=1
    z=x*y
    return z

attribute.count=0

for number in range(1,6):
    print(attribute(number,2))
    
print('Function attribute was called',attribute.count,' times')

hyphenated_word.py
# Mark 29/5/20
# A selection of different methods for extracting the hyphenated words
# in the nouns.txt file

filename = 'nouns.txt'      #Windows
#filename = './nouns.txt'      #Linux

with open(filename,'r') as reader:
    whole_text = reader.read()
words_list = whole_text.split()
reader.close


# for loop
#------------
hy_words_f=[]
for word in words_list:
    if '-' in word:
        hy_words_f.append(word)
        
        print(word)
        
print('for:',len(hy_words_f))

# list comprehension
#-------------------
hy_words_lc=[word for word in words_list if '-' in word]

print('list comprehension:',len(hy_words_lc))

# list filter
#---------------

hy_words_filter=list(filter((lambda word: '-' in word),words_list))

print('list filter:',len(hy_words_filter))


# Fuction attribute
# Creating an antribute for a function that then can be used like
# a static local variable with the function.

def attribute(x,y):
    
    attribute.count+=1
    z=x*y
    return z

attribute.count=0

for number in range(1,6):
    print(attribute(number,2))
    
print('Fn attribute was called',attribute.count,' times')

