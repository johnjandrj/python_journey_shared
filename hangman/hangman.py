# hangman.py
# Team version.
# 4/6/2020

# Hangman game.

# Import modules
import random

# Game variables
wrong_guesses=10
word_len_min=4
word_len_max=13
correct_guesses=''    # Letters that the user has entered that are in the secret word
incorrect_guesses=[]  # Letters that the user has entered that are not in the secret word

def get_word():
    #Function to get secret word
    #Enable 1 of the 2 lines below depending on your operating system.
    #filename = "./nouns.txt"   #Linux / Mac
    filename = 'nouns.txt'      #Windows
    with open(filename,'r') as reader:
        whole_text = reader.read()
    words_list = whole_text.split()
    index  = random.randint(0, len(words_list)-1)
    return words_list[index]

def user_entry():
    # Get a single character from the user. Convert it to lower case
    flag_letterentry=True
    while flag_letterentry:
        char=input('Enter a letter:')
        char=char.lower()
        if len(char)!=1:
            print('Please enter just 1 letter')
        elif 96 < ord(char) < 123:  # Check character between 'a' and 'z'
            flag_letterentry=False  # Entry valid so set flag to exit loop
        else:
            print('Please enter a letter between "a" and "z"')
    return char
            

#------------------------- Game Play -------------------------------
secret_word=get_word()
print(secret_word)

print('_ ' * len(secret_word))

word_complete_flag=False

while wrong_guesses>0 and not word_complete_flag:
    guess=user_entry()
        
    if guess in secret_word:
        correct_guesses=correct_guesses + guess
        word_complete_flag=True
        
        for letter in secret_word:
            if letter in correct_guesses:
                print(letter,end=' ')
                
            else:
                print('_ ',end='')
                word_complete_flag=False
                
    else:
        incorrect_guesses.append(guess)
        wrong_guesses-=1
        
                
    print()            
    print('Incorrect letters tried so far:',incorrect_guesses)
    print('Guesses left:',wrong_guesses)

if word_complete_flag:
    print('Well done you won!')
else:
    print("You've had it!!!! The word was",secret_word)

