#moreforMB.py

# Times tables
for outer in range(1,6):
    for inner in range(1,4):
        print(outer,"x",inner," =  ", end ="")
        product=outer * inner
        print (product,end='   ')
        
    print()
# Modify the code above so it prints the numbers being multiplied
# as well as the results. ie... 1 x 2 = 2 etc.
# Remember you can turn numbers into strings with str(your number)
# and add strings together with +    string1 + string2 + .....    
print('-'*50)

# Counting characters
sentence='Now is the time for all good men to come to the aid of the party'
count_of_o=0

# Can you count how many 'o's there are in sentence?
for char in sentence:
    if char == "o":
        count_of_o +=1
print("There are",count_of_o,"o's", "in this sentence.")
print('Count of character o in the sentence =',count_of_o)
print()
print('-'*50)

# How many 3 letter words are there in sentence ?
word_list=sentence.split(' ')
three_letter_words=0
print(word_list)

for word in word_list:
    if len(word)==3:
        three_letter_words+=1     
print('Count of 3 letter words =',three_letter_words)
        
print('-'*50)
# Can you combine the process of looking for 3 letter words with the search for 'o' and
# count the number of three letter words with an 'o' in them ?

three_letter_with_o=0

for word in word_list:
    if len(word)==3:
        for char in word:
            if char == "o":
                three_letter_with_o +=1
                
print('Count of 3 letter words with "o" =',three_letter_with_o)
        