'''
See if you can write a function to underscore some text as shown by Peter.

So a call to function underscore('Alice In Wonderland') produces this:

Alice In Wonderland
---------------------------

Where the number of underscore character always matches the length of 
the text given.

Do you have the function print the result or get the function to return 
a string containing the original plus the underscore characters? Try it 
both ways.'''

novel="War and Peace"
def under_score1(text):
    l=len(text)
    print(text+"\n"+l*"-")
      
print(under_score1(novel))



novel="War and Peace"
def under_score(text):
    l=len(text)
    op=text+"\n"+l*"-"
    return op

print(under_score(novel))
    
      
novel="War and Peace by Tolstoy"
def under_score2(text):
    l=len(text)   
    return text+"\n"+l*"-"

print(under_score2(novel))

    
    

    



    



 