#You are told that there is a text file which consists of a number of words, one on each line.

#Write a program to create a new text file which consists of those words in the original file which contain
#7 or more letters.

word_list=[] 
final_words=[]
final=[]

filename="mixnouns.txt"
f= open(filename,'r')
whole_text = f.read()
f.close()

word_list = whole_text.split("\n")
print(word_list)
word_list.sort()
print()

filename="long_nouns.txt"
f= open(filename,'w')
for word in word_list:
    if len(word)>=7:
        f.write(word)
        f.write("\n")
        final_words.append(word)     
f.close()
final_words.sort()
print(final_words)
print()

filename="long_nouns.txt"
f= open(filename, 'r')
whole_text =f.read()
f.close()
print(whole_text)
