#Write a program which accepts user input of an integer between 1 and 365 and returns
#the date of that day in 2021, where 1st January is day 1. 

#For example, the input

#132

#should result in the output

#Wednesday, 12th May