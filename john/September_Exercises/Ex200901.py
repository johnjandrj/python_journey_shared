#You are told that there is a text file which consists of a number of words, one on each line.
#Write a program to create a new text file which consists of all the words in the original file,
#but in alphabetic order with no line breaks (ie the words all on the same line), separated by single spaces.

word_list=[]

filename = 'mixnouns.txt'
f= open(filename,'r')
whole_text = f.read()
f.close()
print(whole_text)                    #prints original list as in text file
print()
word_list = whole_text.split("\n")   #splits text at new line chars
print(word_list)                     #prints list as []
print()

word_list.sort()
print(word_list)                     #prints sorted list as []
print()

filename = 'sortnouns.txt'
f= open(filename,'w')
for word in word_list:
    f.write(word)                     #writes sorted list to new text file
    f.write(" ")
    print(word, end=" ")              #prints sorted list as written to file
f.close()








