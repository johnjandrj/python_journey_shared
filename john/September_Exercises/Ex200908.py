#You are told that there is a text file which consists of a number of words, one on each line.
#
#rite a program to create a new text file which consists only of those words in the original
#file which contain any letter no more than once (ie no repeated letters).
word=str()
ans=[]
new_word_list=[]
count=int()

with open("mixnouns.txt", "r") as reader:
    whole_text=reader.read()
    whole_text=whole_text.lower()
    word_list=whole_text.split()
for word in word_list:
    lengw=len(word)
    count=0
    ans=[]
    for n in range(0,lengw):                     #cast word to list ans   
        ans.append(word[n])
    for i in range(0,lengw):                     
        if ans.count(word[i])==1:                #count whether each letter in word is repeated
            count=count+1
    if count==lengw:                             #count must equal word length if letters unique         
        new_word_list.append(ans)
        print(word)
        with open("outfile.txt","a") as appender:#file is appended with word if all letters unique.
            word=word+"\n"
            appender.write(word)
            appender.close()
        