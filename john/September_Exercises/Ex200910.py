#!/usr/bin/python3
#You are told that there is a text file which consists of a number of words, one on each line.

#Write a program to find all words in the file which are anagrams of other words in the file.

new_word_list=[]
new_word=[]
fin_word_list=[]
leng=int()
count=int()


with open("more_anagrams.txt", "r") as reader:
    whole_text=reader.read()
    whole_text=whole_text.lower()
    word_list=whole_text.split()
    leng=len(word_list)
    for word in word_list:            
        new_word_list.append(word)
    new_word_list.sort(key=len)
    fin_word_list=[]
for word in new_word_list:
    new_word=[]
    for letter in word:
        new_word.append(letter)
    fin_word_list.append(new_word)
    count=0
for n in range(0,leng,1):
    for i in range(0,leng,1):    
        if len(fin_word_list[n])!=len(fin_word_list[n]):
            continue
        fin_word_list[n].sort()
        fin_word_list[i].sort()
        if fin_word_list[n]==fin_word_list[i] and (new_word_list[n]!=new_word_list[i]) and n<i:
            count+=1
            print(new_word_list[n],"\t\t\t\t ",new_word_list[i])
print()
print(count,"  anagrams in this text file")
 
                   

    
    



    