#You are told that there is a text file which consists of a number of words, one on each line.

#Write a program to find the longest word in the file.  (If there are two or more of the same length,
 #   choose the first in alphabetic order).
 
word_list=[] 
final_words=[]

filename="nouns.txt"
f= open(filename,'r')
whole_text = f.read()
f.close()
word_list = whole_text.split("\n")
print(word_list)

print()
l=len(word_list)
for i in range (0,l-1):
    for j in range (0,(l-1)):
        if len(word_list[j+1])>=len(word_list[(j)]):
            temp=word_list.pop(j+1)
            word_list.insert((j),temp)
           
print(word_list)

print()
for i in range(0,l-1):
    if len(word_list[i])==len(word_list[0]):
        final_words.append(word_list[i])
final_words.sort()        
print(final_words)

print()
print(final_words[0])


        
