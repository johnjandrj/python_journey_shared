#You are told that there is a text file which consists of a number of words, one on each line.
#Write a program to find out how many words there are.  The output should be "There are <n> words in the file
# with the correct number in place of <n>.

f= open('mixnouns.txt','r')
whole_text = f.read()
f.close()
print("There are",(whole_text.count("\n")+1),"words in the file.")