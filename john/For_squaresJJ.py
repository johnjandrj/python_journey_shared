#! Python 3
#For_squaresJJ.py
#outputs a square grid of all combinations of 2-9 rows and 2-9 columns maximizing square border continuity
#John Johnson 200513


x="|______"
y="|     "
z="_______"

print("MAXIMIZE THE SHELL DISPLAY AREA")
print()
rows=int(input("How many rows would you like? between 2 and 9 please. "))
cols=int(input ("How many columns would you like? between 2 and 9 please. "))


if rows<2 or rows>9 or cols<2 or cols>9:                     #Check for valid inputs
    ans=input('Please only use the specified range between 2 and 9. Any key to try again.')
    rows=cols=0                                        #continuing with these inputs prints nothing.
    

for row in range(rows):                           #Number of rows
    if row==0:        
        for col in range(cols):                   # prints unbroken first line without "|" character.
            print(z, end="")
        print("_")
    else:
        for col in range(cols):                   #prints unbroken horizontal lines for remaider of grid
            print(x, end="")
        print("|")
        
    print("|",end="")        
    for line in range(3):        
        for col in range((cols)):           
            if line==0:                             #prints cell coordinates in line 1 of each cell
                coord=str(row)+":"+str(col)               
                print(coord,"  ", end="")
                print("|",end="")                     
            else:
                print(y,end=" ")                 #prints blank lines in each cell
                if (col+1)==cols:
                    print("|",end="")                              
            
        print("")
for c in range(1,(cols+1)):                           #prints final unbroken horizontal line
    print(x, end="")                                         
                           
if cols !=0:
    print("|")                                   #print last vertical side bar for valid inputs   
                           
               

