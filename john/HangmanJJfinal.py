#!python 3
#HangmanJJ game v computer generated word
#John Johnson
#200530

from random import random

new_word_list=[]
list_ans_word=[]
list_hang_word=[]
count=0

def getword():
    filename = 'nouns.txt'  
    with open(filename,'r') as reader:
        whole_text = reader.read()
        whole_text = whole_text.upper()
        word_list = whole_text.split()        
        for word in word_list:
            if 5<len(word)<10:
                new_word_list.append(word)
        leng=len(new_word_list)      
        num=int(random()*leng-1) 
        string_hang_word=new_word_list[num]         
        for item in string_hang_word:
            list_ans_word.append("_") 
            list_hang_word.append(item)
        print(list_ans_word)
        print()
        print(list_hang_word)                                            #Comment back in for testing
        return list_ans_word

def getletter():    
    flag =True
    global count
    while flag:        
        letter=input("Enter a letter  ")
        letter=letter.upper()
        if len(letter)!=1:
            count+=1
            if count>15:
                letter="x"
                return letter
            print("single letter only")           
        elif 64<ord(letter)<91:                  
            flag=False
        else:
            print('Please enter a letter between "A" and "Z"')
            count+=1
            if count>15:
                letter= "x"
                return letter
    return letter    

def getandtest():
    totlet=""
    global count    
    while list_hang_word!=list_ans_word and count5:              
        letter=getletter()
        letter=letter.upper()
        if len(letter)==1:
            totlet=totlet+letter
        number=0
        for item in list_hang_word:          #loop through hangword to find matches to letter
            if item==letter:  
                list_ans_word[number]=letter                                             #assign chosen letter to the answer word OR NOT
            number+=1                        
        print(list_ans_word, totlet)         #update the answer list on screen
        count+=1                             #number of trys and loop back for more
        print()
        if count>15:
            print("You lose, the word is ",list_hang_word)       
    print("You took",count,"tries to complete the puzzle.\n\nGreater than 15 you are dead.")
    return


#--------------------------------------------------------------------------------------------

list_ans_word = getword()
getandtest()



