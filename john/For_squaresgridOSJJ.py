#! Python 3
#For_squaresgridOSJJ.py
#outputs a square grid of all combinations of 2-9 rows and 2-9 columns maximizing square border continuity
#cell indecies from bottom left
#John Johnson 230513



def input_check(r,c):                                       #Function to check input validity

    if len(r)!=1 or len(c)!=1 :                              #input length must be only 1 character
        print ('Invalid entry, Try again')
        return "bad"    
    elif 49<ord(r)<58 and 49<ord(c)<58:                      # input ASCII values 2 to 9 inclusive    
        return "ok"       
    else:
        print("Out of range or invalid entry. Try again")   #input out of range or not a digit
        return "bad"
       
x="|______"
y="|     "
z=" ______"

print("MAXIMIZE THE SHELL DISPLAY AREA")
print()

rows=input("How many rows would you like? between 2 and 9 please. ")     #Get rows input from caller
print()
cols=input ("How many columns would you like? between 2 and 9 please. ") #Get columns from caller
print()

if (input_check(rows,cols))=="ok":                #run function to check input validity
    rows=int(rows)                                #with input validated convert inputs to integers
    cols=int(cols)   
else:
    rows=cols=0                                   #invalid inputs use default values to exit programme
    
for row in range((rows-1),-1,-1):                 #Number of rows
    if row==rows-1:        
        for col in range(cols):                   # prints unbroken first line without "|" character.
            print(z, end="")
        print("")
    else:
        for col in range(cols):                   #prints unbroken horizontal lines for remaider of grid
            print(x, end="")
        print("|")
        
    print("|",end="")        
    for line in range(3):                          # 4 horiz lines in each cell
        for col in range(cols):           
            if line==0:                            #prints cell coordinates in line 1 of each cell
                coord=str(row)+":"+str(col)               
                print(coord,"  ", end="")
                print("|",end="")                     
            else:
                print(y,end=" ")                   #prints blank lines in each cell plus last "|"
                if (col+1)==cols:
                    print("|",end="")                              
            
        print("")
for col in range(1,(cols+1)):                      #prints final unbroken horizontal line
    print(x, end="")                                         
                           
if cols !=0:
    print("|")                                     #print last vertical side bar for valid inputs   
                           





