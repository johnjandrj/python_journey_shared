# BasicMenu.py

# Mark 14/4/20 with John 15/4/20

# 5 option menu.

print('\t Basic Menu \n')
print('\t 1. Print')
print('\t 2. Save')
print('\t 3. Delete')
print('\t 4. Run')
print('\t 5. Edit')
answer = 'YES'

while answer == 'YES':
    answer = 'no'
    n=input('     Select an option:')

    print()   # print an empty line.

    if n=='1':
    # 1
        print('Print option selected')
    elif n=='2':
    # 2
        print('Save option selected')
    elif n=='3':
    # 3
        print('Delete option selected')
    elif n=='4':
    # 4
        print('Run option selected')
    elif n=='5':
    # 5
        print('Edit option selected')
    else:
        print('You have not chosen a valid option')
        print()
        answer=input('To try again enter YES  ')
        
    print("You have chosen to exit without making a choice from the menu")
    