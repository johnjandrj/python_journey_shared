# Python Program to check Triangle is Valid or Not
# Tom 19/04/2020
# Not exactly what was asked for

a = int(input('Please Enter the First Angle of a Triangle: '))
b = int(input('Please Enter the Second Angle of a Triangle: '))
c = int(input('Please Enter the Third Angle of a Triangle: '))

# checking Triangle is Valid or Not
total = a + b + c

if total == 180:
    print("\nThis is a Valid Triangle")
else:
    print("\nThis is an Invalid Triangle")
