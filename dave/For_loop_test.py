#moreforMB.py

# Times tables
for outer in range(1,6):
    for inner in range(1,4):
        product=outer * inner
        print (outer, 'x' ,inner ,'=' , product,end='       ')
        
    print()
    
# Modify the code above so it prints the numbers being multiplied
# as well as the results. ie... 1 x 2 = 2 etc.
# Remember you can turn numbers into strings with str(your number)
# and add strings together with +    string1 + string2 + .....

print('-'*80)

         # Counting characters
sentence='Now is the time for all good men to come to the aid of the party'



          # Can you count how many 'o's there are in sentence?
count_of_o= ( sentence.count ('o') )
print('Count of character o in the sentence =',count_of_o)
    
print('-'*80) 

          # How many 3 letter words are there in sentence ?
word = []
count_of_o=0
          
word_list=sentence.split(' ')
for word in word_list:
    if len(word) == 3:
        print (word, end = '  ')
        
print()       
for word in word_list:
    if len (word) ==3:
        count_of_o += 1
               
print('Count of 3 letter words =',count_of_o)
        
print('-'*80)

       # Can you combine the process of looking for 3 letter words with the search for 'o' and
       # count the number of three letter words with an 'o' in them ?

three_letter_with_o=0

for word in word_list:
    if len (word) ==3:
        for char in word:
            if char == 'o':
                three_letter_with_o +=1

print('Count of 3 letter words with o =',three_letter_with_o)