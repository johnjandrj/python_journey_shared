#square.py by Dave 22/04/20
#to say whether four random sticks will make a square
#using the list method and loop

rectangle=False
    
while not rectangle :
        
        a=int(input('enter a length for stick a ! :'))
        b=int(input('enter a length for stick b ! :'))
        c=int(input('enter a length for stick c ! :'))
        d=int(input('enter a length for stick d ! :'))
        
        #make a list of the sticks
        
        sticks=[a,b,c,d]
        print (sticks)
        
        #sort sticks into ascending lengths
        
        sticks.sort()
        print (sticks)
        
        #test for whether sticks will make a rectangle
        
        if sticks[0] + sticks[1] + sticks[2] >= sticks[3] :
            print('they will make a rectangle ! :')
            
        elif sticks[0]==sticks[1]==sticks[2]==sticks[3] :
            print('they will make a square ! :')
            
            rectangle=True
            
        else:
             print ('they will not make a rectangle ! :')