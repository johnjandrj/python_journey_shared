#Defining a function examples
#dave  23/5/2020


def hello(a_string):
    print('Hello', a_string)
    
print(hello ('Fred'))


#how to receive information back from a function sing 'return' statement

#def numsquare(num):
  #  square=num * num
    #return square

#listofnumbers=[2,3,4,5,6,7,8,9]

#for number in listofnumbers:
  #  print(numsquare(number))
    
#print()
  
title= "Alice in Wonderland"
def underscore (char):
    l=len (char)
    print(char +' \n' + l * "-")
    
underscore (title)


book = "Treasure Island"
def underscore (char):
    l= len (char)
    v = char  + "\n" + l* '-'
    return v

print (underscore (book))


novel = "readers digest"

print (underscore (novel))
